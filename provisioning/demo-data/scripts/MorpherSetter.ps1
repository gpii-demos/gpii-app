$VerbosePreference = "continue"
$classicShellProc = "ClassicStartMenu"
$classicShell = $null

$syncPath = "$env:HOMEPATH\AppData\Local\GPII-Demo\In-P"

New-Item -Path $syncPath -ItemType 'file'

Start-Process -FilePath "C:\Program Files\Classic Shell\ClassicStartMenu.exe"

while($classicShell -eq $null)
{
    Start-Sleep -Milliseconds 200
    $classicShell = Get-Process -Name $classicShellProc -ErrorAction SilentlyContinue
}

Stop-Process -Name "explorer"

Remove-Item -Path $syncPath