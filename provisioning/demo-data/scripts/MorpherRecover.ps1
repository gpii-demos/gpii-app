$VerbosePreference = "continue"

$classicShellProc = "ClassicStartMenu"
$syncPath = "$env:HOMEPATH\AppData\Local\GPII-Demo\In-P"

while (Test-Path $syncPath) {
    Start-Sleep -Milliseconds 200
}

while ((Get-Process -Name $classicShellProc -ErrorAction SilentlyContinue) -ne $null) {
    Start-Process -FilePath "C:\Program Files\Classic Shell\ClassicStartMenu.exe" -ArgumentList "-exit"
    Start-Sleep -Milliseconds 200
}

Stop-Process -Name "explorer"