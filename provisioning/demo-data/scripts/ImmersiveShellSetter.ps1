<#
  .
#>

$VerbosePreference = "continue"

$GPII_Demo_Path = Join-Path $env:LOCALAPPDATA "GPII-Demo"

Write-Verbose("Starting $PSCommandPath and set GPII_Demo_Path: $GPII_Demo_Path")

$TabletModeScript = Join-Path $GPII_Demo_Path "scripts\TabletView.ahk"
Write-Verbose("Launching tabletModeScript : $TabletModeScript")
Start-Process -FilePath $TabletModeScript 0
