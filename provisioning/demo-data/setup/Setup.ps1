<#
  .
#>

$VerbosePreference = "continue"
$GPII_Demo_Path = Join-Path $env:LOCALAPPDATA "GPII-Demo"
Write-Verbose("Starting $PSCommandPath and set GPII_Demo_Path: $GPII_Demo_Path")

$demoZip = Join-Path $env:TEMP "demo.zip"
$demoDir = Join-Path $env:TEMP "demo"

choco install --yes git
refreshenv
Remove-Item $demoDir -Recurse -Force -Confirm:$false
git clone https://gitlab.com/gpii-demos/gpii-app.git $demoDir

$buildScript = Join-Path $demoDir "\provisioning\Build.ps1"
Write-Verbose("Launching build script located at $buildScript")
Invoke-Expression $buildScript
