<#
  This script installs the dependencies for gpii-app:
  1) installs nodejs via chocolatey
  2) installs the gpii-app npm dependencies
  3) runs the provisioning scripts from the (gpii-)windows repo

  If run via a tool (like vagrant) which moves this script to somewhere different
  than its original location within the gpii-app repository, the parameter
  "-originalBuildScriptPath" should be provided, with the original location of the
  script
#>

param (
    [string]$originalBuildScriptPath = (Split-Path -parent $PSCommandPath) # Default to script path.
)

Function Execute-Script {
  Param (
    [string] $Filename,
    [string] $URI
  )

  try {
    $local_script = Join-Path $originalBuildScriptPath $Filename
    Write-Verbose "Running windows script: $local_script"
    iwr $URI -UseBasicParsing -OutFile $local_script
    Invoke-Expression $local_script
  } catch {
    Write-OutPut "$Filename obtained from $URI : FAILED"
    Throw "$Filename obtained from $URI : FAILED"
    exit 1
  }
}

# Turn verbose on, change to "SilentlyContinue" for default behaviour.
$VerbosePreference = "continue"

# Store the parent folder of the script (root of the repo) as $mainDir
############
$mainDir = (get-item $originalBuildScriptPath).parent.FullName
Write-OutPut "mainDir set to: $($mainDir)"

# TODO: We should add this to a function or reduce to oneline.
$bootstrapModule = Join-Path $originalBuildScriptPath "Provisioning.psm1"
iwr https://gitlab.com/gpii-demos/windows/raw/demo-mainstream/provisioning/Provisioning.psm1 -UseBasicParsing -OutFile $bootstrapModule
Import-Module $bootstrapModule -Verbose -Force

# # Run all the windows provisioning scripts
# ############
# TODO: Create function for downloading scripts and executing them.
$windowsBootstrapURL = "https://gitlab.com/gpii-demos/windows/raw/demo-mainstream/provisioning"

Execute-Script "Chocolatey.ps1" "$windowsBootstrapURL/Chocolatey.ps1"
Execute-Script "Win10-Morpher.ps1" "$windowsBootstrapURL/Win10-Morpher.ps1"
Execute-Script "Win10-Simplification.ps1" "$windowsBootstrapURL/Win10-Simplification.ps1"
Execute-Script "Npm.ps1" "$windowsBootstrapURL/Npm.ps1"

$npm = "npm" -f $env:SystemDrive
Invoke-Command $npm "install" $mainDir

try {
    $tests = Join-Path $originalBuildScriptPath "Tests.ps1"
    $fullPath = Join-Path $originalBuildScriptPath "../node_modules/gpii-windows/provisioning/"
    $args = "-originalBuildScriptPath $fullPath"
    Write-OutPut "Running windows script: $tests"
    iwr "$windowsBootstrapURL/Tests.ps1" -UseBasicParsing -OutFile $tests
    Invoke-Expression "$tests $args"
} catch {
    Write-OutPut "Tests.ps1 FAILED"
    exit 1
}
